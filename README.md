# Getting Started

# Deploy

Activate profile
`$ set spring_profile_active=<profile>`

local run
`$ mvnw.cmd spring-boot:run`

local run with profile
`$ mvnw.cmd spring:boot:run -Dspring-boot.run.profiles=development`

run final solution
`$ java -jar target/rest_api-<version>.jar --spring.profiles.active=development`

#build
clean target
`$ ./mvnw clean`

target build
`$ ./mvnw package`

Docker build


# CI/CD
`$ sudo apk add gitlab-runner`

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "6UdASyj_X4kzRSbSdsLm" \
  --executor "shell" \
  --docker-image alpine:latest \
  --description "docker-lab-node1" \
  --tag-list "develop_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"



  sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "6UdASyj_X4kzRSbSdsLm" \
  --executor "shell" \
  --docker-image alpine:latest \
  --description "docker-lab-node1" \
  --tag-list "production_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"