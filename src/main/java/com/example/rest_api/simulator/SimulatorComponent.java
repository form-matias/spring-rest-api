package com.example.rest_api.simulator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SimulatorComponent {
    
    @Value("100")
    private int maxRandom;

    @Value("3000")
    private int blinkMillis;

    public int getMaxRandom() {
        return maxRandom;
    }

    public void setMaxRandom(int maxRandom) {
        this.maxRandom = maxRandom;
    }

    public int getBlinkMillis() {
        return blinkMillis;
    }

    public void setBlinkMillis(int blinkMillis) {
        this.blinkMillis = blinkMillis;
    }
    
    

}
