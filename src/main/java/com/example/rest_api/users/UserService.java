package com.example.rest_api.users;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;

@Service
public class UserService {
    
    @Autowired
    private UsersRepository repo;

    @ModelAttribute("users")
    public List<Users> findAll(){
        return (List<Users>) repo.findAll();
    }

}
